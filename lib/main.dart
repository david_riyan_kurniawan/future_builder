import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  HomePage({super.key});
  List allUser = [];

  Future getallUser() async {
    try {
      var response = await http.get(Uri.parse('https://reqres.in/api/users'));
      List data = (json.decode(response.body) as Map<String, dynamic>)['data'];
      data.forEach((element) {
        allUser.add(element);
      });
      print(allUser);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FUTURE BUILDER'),
      ),
      body: FutureBuilder(
        future: getallUser(),
        builder: (context, snapshot) {
          //snapshot berguna untuk mengecek apakah dia loading untuk mendapatkan data dari database
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: Text('Loading..'));
          } else {
            return ListView.builder(
              itemCount: allUser.length,
              itemBuilder: (context, index) => ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.grey[300],
                  backgroundImage: NetworkImage(allUser[index]['avatar']),
                ),
                title: Text(
                    '${allUser[index]['first_name']} ${allUser[index]['last_name']}'),
                subtitle: Text("${allUser[index]['email']}"),
              ),
            );
          }
        },
      ),
    );
  }
}
